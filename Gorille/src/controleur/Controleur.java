package gorille.controleur;

import gorille.modele.*;
import gorille.vue.*;

import javax.swing.JFrame;

import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import java.util.ArrayList;

public class Controleur implements KeyListener {
	public static void main(String[] args) {
		JFrame fenetre = new JFrame();
		fenetre.setSize(800, 500);
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Controleur c = new Controleur(fenetre);
		fenetre.setVisible(true);
		while(c.tourner()) {
			//
		}
		System.exit(0);
	}
	private Gorille gorille;
	private ArrayList<Femme> femmes;
	private Vieille vieille;
	private Juge juge;
	private Vue vue;
	private boolean continuer;
	private long dureeFrame;
	private double distanceAccroche = 20d;
	private Musique musique;
	public Controleur(JFrame fenetre) {
		fenetre.addKeyListener(this);
		this.gorille = new Gorille();
		this.femmes = new ArrayList<Femme>();
		this.vieille = new Vieille();
		this.juge = new Juge();
		this.vue = new Vue(this.gorille, this.femmes, this.vieille, this.juge);
		fenetre.setContentPane(this.vue);
		this.dureeFrame = 1000l / 60l;
		this.continuer = true;
		this.femmes.add(new Femme(this.gorille));
		this.femmes.add(new Femme(this.gorille));
		this.musique = new Musique("/res/musique.wav");
		this.musique.loop();
	}
	public boolean tourner() {
		long debut = System.currentTimeMillis();
		for(int i = 0; i < this.femmes.size(); i++) {
			this.femmes.get(i).bouger();
		}
		this.vieille.bouger();
		this.juge.bouger();
		this.gorille.bouger();
		if(this.vue.pret()) this.vue.repaint();
		try {
			Thread.sleep(this.dureeFrame - System.currentTimeMillis() + debut);
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			return this.continuer;
		}
	}
	@Override
	public void keyPressed(KeyEvent e) {
		switch(e.getKeyCode()) {
		case KeyEvent.VK_LEFT:
			this.gorille.bougerGauche();
			break;
		case KeyEvent.VK_RIGHT:
			this.gorille.bougerDroite();
			break;
		case KeyEvent.VK_UP:
			this.gorille.sauter();
			break;
		case KeyEvent.VK_DOWN:
			this.gorille.descendre();
			break;
		case KeyEvent.VK_SPACE:
			if(this.gorille.victime() != this.juge && Math.abs(this.juge.x() - this.gorille.x()) < this.distanceAccroche) {
				this.gorille.saisir(this.juge);
			} else {
				boolean trouve = false;
				for(int i = 0; i < this.femmes.size(); i++) {
					if(this.gorille.victime() != this.femmes.get(i) && Math.abs(this.femmes.get(i).x() - this.gorille.x()) < this.distanceAccroche) {
						this.gorille.saisir(this.femmes.get(i));
						trouve = true;
						break;
					}
				}
				if(!trouve) {
					if(this.gorille.victime() != this.vieille && Math.abs(this.vieille.x() - this.gorille.x()) < this.distanceAccroche) {
						this.gorille.saisir(this.vieille);
					} else {
						this.gorille.saisir(null);
					}
				}
			}
			break;
		case KeyEvent.VK_ESCAPE:
			this.continuer = false;
			break;
		}
	}
	@Override
	public void keyReleased(KeyEvent e) {
		this.gorille.arreterMouvement();
	}
	@Override
	public void keyTyped(KeyEvent e) {}
}
