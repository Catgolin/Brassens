package gorille.modele;

public class Femme extends Personnage {
	private Gorille gorille;
	private double vitesse = 0.5;
	public Femme(Gorille g) {
		super(100, 100, 10, 20, (int)(Math.random() * 100), 0);
		this.gorille = g;
	}
	public void bouger() {
		if(this.gorille.x() + this.gorille.largeur() < this.x() || this.gorille.x() + this.gorille.largeur() == 0) {
			this.setX(this.x() + this.vitesse);
			if(this.largeur() < 0) {this.inverserLargeur();}
		} else {
			this.setX(this.x() - this.vitesse);
			if(this.largeur() > 0) {this.inverserLargeur();}
		}
		this.setY(0);
	}
}
