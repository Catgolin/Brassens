package gorille.modele;

public class Gorille extends Personnage {
	private Personnage victime;
	private double vY;
	private double vX;
	private static double gravite = -0.1;
	private double mainX = 1;
	private double mainY = 1;
	private double vitesseSaut = 1;
	private double aX = 1;
	private boolean arreterMouvement;
	public Gorille() {
		super(100, 100, 20, 20, 20, 0);
	}
	public void saisir(Personnage p) {
		if(this.victime != null) {
			this.victime.renverser();
		}
		this.victime = p;
		if(this.victime != null) {
			this.victime.renverser();
		}
	}
	public Personnage victime() {return this.victime;}
	public void bouger() {
		if(this.setY(this.y() + this.vY)) {
			this.vY += this.gravite;
		} else {
			this.vY = 0;
			if(this.arreterMouvement) {
				this.vX = 0;
				this.arreterMouvement = false;
			}
		}
		this.setX(this.x() + this.vX);
		if(this.vX * this.largeur() < 0) {
			this.inverserLargeur();
		}
		if(this.victime != null) {
			this.victime.setX(this.x()+this.mainX);
			this.victime.setY(this.y()+(this.victime.hauteur() < 0 ? - this.victime.hauteur() + this.mainY : this.mainY));
		}
	}
	public void bougerGauche() {
		this.vX = this.vX > 0 ? -this.aX : this.vX - this.aX;
	}
	public void bougerDroite() {
		this.vX = this.vX > 0 ? this.vX + this.aX : this.aX;
	}
	public void arreterMouvement() {
		this.arreterMouvement = true;
	}
	public void sauter() {
		this.vY = this.vitesseSaut;
	}
	public void descendre() {
		this.vY = -this.vitesseSaut;
	}
}
