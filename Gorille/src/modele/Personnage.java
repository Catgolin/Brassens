package gorille.modele;

import java.awt.Image;

public abstract class Personnage {
	private double x;
	private double y;
	private double largeur;
	private double hauteur;
	private static double xMax;
	private static double yMax;
	public Personnage(double xMax, double yMax, double largeur, double hauteur, double x, double y) {
		this.xMax = xMax;
		this.yMax = yMax;
		this.largeur = largeur;
		this.hauteur = hauteur;
		this.x = x;
		this.y = y;
	}
	protected boolean setX(double x) {
		if(x + this.largeur < 0) {
			this.x = - this.largeur;
			return false;
		} else if(x < 0) {
			this.x = 0;
			return false;
		} else if(this.xMax < Integer.MAX_VALUE && x + this.largeur > this.xMax) {
			this.x = this.xMax - this.largeur;
			return false;
		} else if(this.xMax < Integer.MAX_VALUE && x > this.xMax) {
			this.x = this.xMax;
			return false;
		} else {
			this.x = x;
			return true;
		}
	}
	protected boolean setY(double y) {
		if(y + this.hauteur <= 0) {
			this.y = this.hauteur;
			return false;
		} else if(y < 0) {
			this.y = 0;
			return false;
		} else if(this.yMax < Integer.MAX_VALUE && y + this.hauteur >= this.yMax) {
			this.y = this.yMax - this.hauteur;
			return false;
		} else if(this.yMax < Integer.MAX_VALUE && y >= this.yMax) {
			this.y = this.yMax;
			return false;
		} else {
			this.y = y;
			return true;
		}
	}
	protected void inverserLargeur() {
		this.largeur = - this.largeur;
	}
	protected void renverser() {
		double largeur = this.largeur;
		this.largeur = this.hauteur;
		this.hauteur = largeur;
	}
	public double x() {return this.x;}
	public double y() {return this.y;}
	public double largeur() {return this.largeur;}
	public double hauteur() {return this.hauteur;}
	public abstract void bouger();
}
