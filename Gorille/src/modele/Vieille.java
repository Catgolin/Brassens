package gorille.modele;

public class Vieille extends Personnage {
	public Vieille() {
		super(100, 100, 10, 20, (int)(Math.random() * 100), 0);
	}
	public void bouger() {
		this.setY(0);
	}
}
