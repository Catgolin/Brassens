package gorille.vue;

import gorille.modele.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.ImageIcon;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Image;

import java.util.ArrayList;

public class Vue extends JPanel {
	private Gorille gorille;
	private ArrayList<Femme> femmes;
	private Vieille vieille;
	private Juge juge;
	private boolean pret;
	private Image imageGorille;
	private Image imageFille;
	private Image imageVieille;
	private Image imageJuge;
	private Image filleR;
	private Image vieilleR;
	private Image jugeR;
	public Vue(Gorille g, ArrayList<Femme> f, Vieille v, Juge j) {
		this.gorille = g;
		this.femmes = f;
		this.vieille = v;
		this.juge = j;
		this.pret = true;
		this.imageGorille = new ImageIcon(this.getClass().getResource("/res/gorille.png")).getImage();
		this.imageFille = new ImageIcon(this.getClass().getResource("/res/fille.png")).getImage();
		this.imageVieille = new ImageIcon(this.getClass().getResource("/res/vieille.png")).getImage();
		this.imageJuge = new ImageIcon(this.getClass().getResource("/res/juge.png")).getImage();
		this.filleR = new ImageIcon(this.getClass().getResource("/res/filleR.png")).getImage();
		this.jugeR = new ImageIcon(this.getClass().getResource("/res/jugeR.png")).getImage();
		this.vieilleR = new ImageIcon(this.getClass().getResource("/res/vieilleR.png")).getImage();
	}
	public boolean pret() {return this.pret;}
	@Override
	public void paintComponent(Graphics g) {
		this.pret = false;
		Component c = (Component)this;
		while(!(c instanceof JFrame)) {
			c = c.getParent();
		}
		JFrame fenetre = (JFrame) c;
		fenetre.setSize(fenetre.getHeight(), fenetre.getHeight());
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		this.afficher(g, this.gorille, this.imageGorille);
		for(int i = 0; i < this.femmes.size(); i++) {
			Image image = this.femmes.get(i).largeur() > this.femmes.get(i).hauteur() ? this.filleR : this.imageFille;
			this.afficher(g, this.femmes.get(i), image);
		}
		this.afficher(g, this.vieille, this.vieille.largeur() > this.vieille.hauteur() ? this.vieilleR : this.imageVieille);
		this.afficher(g, this.juge, this.juge.largeur() > this.juge.hauteur() ? this.jugeR : this.imageJuge);
		this.pret = true;
	}
	private void afficher(Graphics g, Personnage p, Image i) {
		int x = (int)(p.x() / 100d * (double)this.getWidth());
		int y = (int)(p.y() / 100d * (double)this.getHeight());
		int l = (int)(p.largeur() / 100d * (double)this.getWidth());
		int h = (int)(p.hauteur() / 100d * (double)this.getHeight());
		g.drawImage(i, x, this.getHeight() - y - h, l, h, this);
	}
}
