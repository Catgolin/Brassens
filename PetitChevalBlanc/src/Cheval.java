package petitChevalBlanc.modele;

public class Cheval {
	private int nbLignes;
	private int ligne;
	private int avance;
	private int vies;
	private static int longueur = 20;
	public Cheval(int nbLignes, int vies) {
		this.nbLignes = nbLignes;
		this.ligne = nbLignes / 2;
		this.avance = 0;
		this.vies = vies;
	}
	public int vies() {return this.vies;}
	public int avance() {return this.avance;}
	public int ligne() {return this.ligne;}
	public int longueur() {return Cheval.longueur;}
	public void frapper() {this.vies--;}
	public void monter() {this.ligne -= this.ligne > 0 ? 1 : 0;}
	public void descendre() {this.ligne += this.ligne < this.nbLignes ? 1 : 0;}
	public void avancer() {this.avance++;}
}
