package petitChevalBlanc.controleur;

import petitChevalBlanc.modele.Cheval;
import petitChevalBlanc.modele.Eclair;

import petitChevalBlanc.vue.Vue;
import petitChevalBlanc.vue.Musique;

import java.util.ArrayList;

import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class Controleur implements KeyListener {
	public static void main(String[] args) {
		JFrame fenetre = new JFrame();
		fenetre.setSize(800, 500);
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Controleur c = new Controleur(fenetre, 5);
		fenetre.setVisible(true);
		while(c.tourner()) {
			//
		}
		c.afficherFin(fenetre);
	}
	private Vue vue;
	private Cheval cheval;
	private ArrayList<Eclair> eclairs;
	private long dureeFrame;
	private int lignes;
	private Musique musique;
	private Musique eclair;
	public Controleur(JFrame fenetre, int nbLignes) {
		this.musique = new Musique("/res/musique.wav");
		this.eclair = new Musique("/res/eclair.wav");
		this.musique.loop();
		this.lignes = nbLignes;
		this.eclairs = new ArrayList<Eclair>();
		this.cheval = new Cheval(this.lignes, 3);
		this.vue = new Vue(this.cheval, this.eclairs, this.lignes, 100);
		fenetre.addKeyListener(this);
		fenetre.setContentPane(this.vue);
		this.dureeFrame = 1000 / 30;
	}
	public boolean tourner() {
		long debutFrame = System.currentTimeMillis();
		this.cheval.avancer();
		for(int i = 0; i < this.eclairs.size(); i++) {
			this.eclairs.get(i).tourner();
			if(this.eclairs.get(i).force() <= 0) {
				this.eclairs.remove(i);
			}
			if(this.eclairs.get(i).ligne() == this.cheval.ligne()) {
				int x = this.eclairs.get(i).avance();
				if(x > this.cheval.avance() && x < this.cheval.avance() + this.cheval.longueur()) {
					this.cheval.frapper();
					this.eclairs.get(i).vider();
					this.eclair.start();
					this.vue.eclair();
				}
			}
		}
		this.eclairs.add(new Eclair((int)(Math.random() * this.lignes), (int)(Math.random() * 1000) + this.cheval.avance()));
		if(this.vue.pret()) {
			this.vue.repaint();
		}
		try {
			Thread.sleep(this.dureeFrame - System.currentTimeMillis() + debutFrame);
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			return this.cheval.vies() > 0;
		}
	}
	public void afficherFin(JFrame fenetre) {
		JLabel message = new JLabel("C'était un petit cheval blanc...", JLabel.CENTER);
		fenetre.setContentPane(message);
		try {Thread.sleep(1000);}catch(Exception e) {}
		fenetre.repaint();
		try {Thread.sleep(1000);}catch(Exception e) {}
		fenetre.validate();
		message.repaint();
	}
	@Override
	public void keyPressed(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_UP && this.cheval.vies() > 0) {
			this.cheval.monter();
		} else if(e.getKeyCode() == KeyEvent.VK_DOWN && this.cheval.vies() > 0) {
			this.cheval.descendre();
		}
	}
	@Override
	public void keyTyped(KeyEvent e) {}
}
