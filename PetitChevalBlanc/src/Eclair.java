package petitChevalBlanc.modele;

public class Eclair {
	private static int forceInitiale = 50;
	private int ligne;
	private int avance;
	private int force;
	private boolean vider = false;
	public Eclair(int x, int y) {
		this.ligne = x;
		this.avance = y;
		this.force = Eclair.forceInitiale;
	}
	public int ligne() {return this.ligne;}
	public int avance() {return this.avance;}
	public int force() {return this.force;}
	public void vider() {this.vider = true;}
	public void tourner() {
		this.force = this.vider ? 0 : this.force / 2;
	}
}
