package petitChevalBlanc.vue;

import java.io.InputStream;
import java.io.BufferedInputStream;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;

public class Musique {
	Clip clip;
	public Musique(String lien) {
		try {
			InputStream source = this.getClass().getResourceAsStream(lien);
			InputStream tampon = new BufferedInputStream(source);
			AudioInputStream entree = AudioSystem.getAudioInputStream(tampon);
			DataLine.Info info = new DataLine.Info(Clip.class, entree.getFormat());
			this.clip = (Clip)AudioSystem.getLine(info);
			this.clip.open(entree);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void loop() {
		this.clip.loop(Clip.LOOP_CONTINUOUSLY);
	}
	public void start() {
		if(!this.clip.isRunning()) {
			this.clip.stop();
			this.clip.setFramePosition(0);
			this.clip.start();
		}
	}
	public void stop() {
		this.clip.stop();
	}
}
