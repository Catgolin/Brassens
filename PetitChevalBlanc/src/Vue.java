package petitChevalBlanc.vue;

import petitChevalBlanc.modele.Cheval;
import petitChevalBlanc.modele.Eclair;

import javax.swing.JPanel;
import javax.swing.ImageIcon;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Color;

import java.util.ArrayList;

public class Vue extends JPanel {
	private int nbLignes;
	private int vision;
	private Cheval cheval;
	private ArrayList<Eclair> eclairs;
	private boolean pret;
	private Image imageCheval;
	private Image imageVie;
	private Image imageEclair;
	private boolean eclair = false;
	public Vue(Cheval cheval, ArrayList<Eclair> eclairs, int nbLignes, int vision) {
		this.cheval = cheval;
		this.vision = vision;
		this.nbLignes = nbLignes;
		this.eclairs = eclairs;
		this.imageCheval = new ImageIcon(this.getClass().getResource("/res/cheval.gif")).getImage();
		this.imageVie = new ImageIcon(this.getClass().getResource("/res/vie.png")).getImage();
		this.imageEclair = new ImageIcon(this.getClass().getResource("/res/eclair.png")).getImage();
		this.pret = true;
	}
	public boolean pret() {return this.pret;}
	@Override
	public void paintComponent(Graphics g) {
		this.pret = false;
		long debut = System.currentTimeMillis();
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		g.setColor(Color.WHITE);
		int x = this.getWidth() / this.vision;
		int y = this.getHeight() / this.nbLignes * this.cheval.ligne();
		int largeur = (int)((double)(this.getWidth()) / (double)(this.vision) * (double)this.cheval.longueur());
		g.drawImage(this.imageCheval, x, y, largeur, this.getHeight() / this.nbLignes, this);
		g.setColor(Color.YELLOW);
		for(int i = 0; i < this.eclairs.size(); i++) {
			x = this.getWidth() / this.vision * (this.eclairs.get(i).avance() - this.cheval.avance());
			x += this.getWidth() / this.vision / 2;
			y = this.getHeight() / this.nbLignes * this.eclairs.get(i).ligne();
			y += this.getHeight() / this.nbLignes / 2;
			largeur = this.imageEclair.getWidth(this);
			g.drawImage(this.imageEclair, x, 0, largeur, y, this);
		}
		g.setColor(Color.RED);
		for(int i = 0; i < this.cheval.vies(); i++) {
			g.drawImage(this.imageVie, i * 20 + 10, 10, 10, 10, this);
		}
		if(this.eclair) {
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
			this.eclair = false;
		}
		this.pret = true;
	}
	public void eclair() {
		this.eclair = true;
	}
}
